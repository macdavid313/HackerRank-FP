# [HackerRank Functional Programming](https://www.hackerrank.com/domains/fp)

My solutions for HackerRank FP domain, (mainly) written in Common Lisp.