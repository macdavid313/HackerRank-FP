(defun full-of-color-p (seq)
  (declare (type simple-string seq)
	   (optimize (speed 3) (safety 0) (space 0)))
  (let ((table (make-hash-table :test 'eq :size 4)))
    (declare (type hash-table table))
    (dolist (color '(#\R #\G #\Y #\B))
      (setf (gethash color table) 0))
    (labels ((diffp ()
	       (and
		(<= (abs (- (the fixnum (gethash #\R table))
			    (the fixnum (gethash #\G table))))
		    1)
		(<= (abs (- (the fixnum (gethash #\Y table))
			    (the fixnum (gethash #\B table))))
		    1))))
      (loop for color across seq
	 do (progn
	      (incf (the fixnum (gethash color table)))
	      (unless (diffp) (return "False"))))
      (if (and (= (the fixnum (gethash #\R table))
		  (the fixnum (gethash #\G table)))
	       (= (the fixnum (gethash #\Y table))
		  (the fixnum (gethash #\B table))))
	  "True"
	  "False"))))

(defun main ()
  (let ((times (parse-integer (read-line))))
    (dotimes (i times)
      (let ((colors (read-line)))
	(format t "~a~%" (full-of-color-p colors))))))

(main)
