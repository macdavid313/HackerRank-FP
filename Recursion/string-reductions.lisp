(defparameter *chars*
  (let ((table (make-hash-table :size 26 :test #'eq)))
    (loop for char from 97 to 122
       do (setf (gethash (code-char char) table) nil))
    table))

(defun str-reduction (str)
  (declare (type simple-string str)
	   (optimize (space 0) (safety 0) (speed 3)))
  (let ((result (make-array 0 :element-type 'character :adjustable t :fill-pointer 0))
	(len (length str)))
    (declare (type fixnum len))
    (labels ((iter (index)
	       (declare (type fixnum index))
	       (cond ((= index len) result)
		     ((gethash (char str index) *chars*) (iter (the fixnum (+ 1 index))))
		     (t
		      (vector-push-extend (char str index) result)
		      (setf (gethash (char str index) *chars*) t)
		      (iter (the fixnum (+ 1 index)))))))
      (iter 0))))

(defun main ()
  (let ((str (read-line)))
    (format t "~a~%" (str-reduction str))))

(main)
