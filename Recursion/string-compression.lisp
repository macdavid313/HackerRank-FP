(defun str-compress (str)
  (declare (type simple-string str)
	   (optimize (space 0) (safety 0) (speed 3)))
  (let ((result (make-array 0 :element-type 'character
			    :adjustable t :fill-pointer 0))
	(len (length str)))
    (labels ((helper (char count)
	       (declare (type character char) (type fixnum count))
	       (vector-push-extend char result)
	       (when (> count 1)
		 (let ((count (write-to-string count)))
		   (declare (type simple-string count))
		   (loop for c across count
			do (vector-push-extend c result)))))
	     (iter (next-index char count)
	       (declare (type fixnum next-index count)
			(type character char))
	       (cond (;; recursion over
		      (= next-index len)
		      (helper char count)
		      result)
		     (;; count the same character
		      (char= char (char str next-index))
		      (iter (the fixnum (1+ next-index)) char (the fixnum (1+ count))))
		     (;; count over
		      (char/= char (char str next-index))
		      (helper char count)
		      (iter (the fixnum (1+ next-index))
			    (the character (char str next-index))
			    1)))))
      (iter 1 (char str 0) 1))))

(defun main ()
  (let ((str (read-line)))
    (format t "~a~%" (str-compress str))))

(main)
