(defun str-mingling (str1 str2)
  (declare (type simple-string str1 str2)
	   (optimize (space 0) (safety 0) (speed 3)))
  (let* ((len (length str1))
	 (result (make-array (* 2 len) :element-type 'character)))
    (declare (type simple-string result))
    (labels ((iter (index)
	       (declare (type fixnum index))
	       (if (= index len)
		   result
		   (let ((index% (* 2 index)))
		     (declare (type fixnum index%))
		     (progn
		       (setf (char result index%) (char str1 index))
		       (setf (char result (+ index% 1)) (char str2 index))
		       (iter (the fixnum (+ 1 index))))))))
      (iter 0))))

(defun main ()
  (let ((str1 (read-line))
	(str2 (read-line)))
    (format t "~a~%" (str-mingling str1 str2))))

(main)
