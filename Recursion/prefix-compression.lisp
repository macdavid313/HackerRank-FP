(defun str-prefix (str1 str2)
  (declare (type simple-string str1 str2)
	   (optimize (space 0) (safety 0) (speed 3)))
  (let ((len1 (length str1))
	(len2 (length str2))
	(prefix (make-array 0 :element-type 'character :adjustable t :fill-pointer 0)))
    (declare (type fixnum len1 len2))
    (labels ((iter (index count)
	       (declare (type fixnum index count))
	       (cond ((or
		       (= index len1)
		       (= index len2)
		       (char/= (char str1 index) (char str2 index)))
		      (values prefix index
			      (the fixnum (- len1 index))
			      (the fixnum (- len2 index))))
		     (t
		      (vector-push-extend (char str1 index) prefix)
		      (iter (the fixnum (+ 1 index)) (the fixnum (+ 1 count)))))))
      (iter 0 0))))

(defun main ()
  (let ((str1 (read-line))
	(str2 (read-line)))
    (multiple-value-bind
	  (prefix index len1 len2) (str-prefix str1 str2)
      (let ((tail1 (if (zerop len1) "" (subseq str1 index)))
	    (tail2 (if (zerop len2) "" (subseq str2 index))))
	(format t "~a ~a~%~a ~a~%~a ~a~%"
		index prefix len1 tail1 len2 tail2)))))

(main)
