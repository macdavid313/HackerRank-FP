(defun factorial (n)
  (declare (type fixnum n)
	   (optimize (speed 3) (safety 0) (space 0)))
  (labels ((iter (current count)
	     (declare (type fixnum current count))
	     (if (> count n)
		 current
		 (iter (the fixnum (* current count))
		       (the fixnum (+ 1 count))))))
    (iter 1 1)))

(let ((divisors (loop for i from 0 to 9 collect (factorial i)))
      (counts (loop for i from 0 to 9 collect i)))
  (defun my-exp (n)
    (declare (type real n))
    (coerce (apply #'+
		   (mapcar #'(lambda (divisor count)
			       (/ (expt n count) divisor))
			   divisors counts))
	    'double-float)))

(defun main ()
  (let ((times (read)))
    (dotimes (i times)
      (format t "~,5f~%" (my-exp (read))))))

(main)
