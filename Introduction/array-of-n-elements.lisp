(let* ((x (parse-integer (read-line)))
       (lst (loop for i from 1 to x collect x)))
  (format t "~A~%" (length lst)))
