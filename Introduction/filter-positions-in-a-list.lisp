(let ((result (make-array 0 :element-type 'simple-string
			  :adjustable t :fill-pointer 0)))
  (loop for line = (read-line T nil)
     while line do (vector-push-extend (parse-integer line) result))
  (let* ((len (length result))
	 (len-lst (loop for i from 0 to (1- len) collect i))
	 (filtered-lst (map 'list #'(lambda (x) (elt result x))
			    (delete-if (complement #'oddp) len-lst))))
    (format t "~{~A~^~%~}~%" filtered-lst)))
