(let (result)
  (loop for line = (read-line T nil)
     while line do (push (abs (parse-integer line)) result)
     finally (format t "~{~A~^~%~}~%" (nreverse result))))
