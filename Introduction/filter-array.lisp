(let ((delimiter (parse-integer (read-line)))
      (result nil))
  (loop for line = (read-line T nil)
     while line do (push (parse-integer line) result)
     finally (setf result (nreverse result)))
  (format t "~{~A~^~%~}~%"
	  (delete-if (complement #'(lambda (x) (< x delimiter))) result)))

