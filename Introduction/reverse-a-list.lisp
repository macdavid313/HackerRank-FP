(let (result)
  (loop for line = (read-line T nil)
     while line do (push line result)
     finally (format t "~{~A~^~%~}~%" result)))
