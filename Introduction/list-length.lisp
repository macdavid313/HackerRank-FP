(let ((count 0))
  (loop for line = (read-line T nil)
     while line do (incf count)
     finally (format t "~A~%" count)))
