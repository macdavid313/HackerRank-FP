(let (result)
  (loop for line = (read-line T nil)
     while line do (push (parse-integer line) result))
  (format t "~A~%" (apply #'+ (delete-if (complement #'oddp) result))))

